function sendMsg() {
    var phoneNumber = document.getElementById('phone').value;
    console.log(phoneNumber);

    var xhr = new XMLHttpRequest();
    xhr.open("post", "/signup/send/sms", true);
    xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    xhr.send(JSON.stringify({'toNumber': phoneNumber}));
}

function sendMail() {
    var mail = document.getElementById('mail').value;
    console.log(mail);

    var xhr = new XMLHttpRequest();
    xhr.open("post", "/signup/send/mail", true);
    xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    xhr.send(JSON.stringify({'toMail': mail}));
}

function send() {
    var phoneNumber = document.getElementById('phone').value;
    var mail = document.getElementById('mail').value;

    if (phoneNumber != '') {
        sendMsg();
    } else if (mail != '') {
        sendMail();
    } else {
        console.log("mail and phone are null");
    }
}