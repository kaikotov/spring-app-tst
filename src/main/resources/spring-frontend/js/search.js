function search() {
    var queryStr = document.getElementById('queryStr').value;
    console.log(queryStr);

    var xhr = new XMLHttpRequest();
    xhr.open("get", "/search/" + queryStr, true);
    xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    xhr.send();
}