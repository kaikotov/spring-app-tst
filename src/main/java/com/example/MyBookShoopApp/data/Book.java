package com.example.MyBookShoopApp.data;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "books")
@Data
@ToString
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private String priceOld;
    private String price;

    @ManyToOne
    @JoinColumn(name = "author_id", referencedColumnName = "id")
    private Author author;

    private Integer sale;


    @OneToMany(mappedBy = "book")
    private List<Genre> genres = new ArrayList<>();

    private Integer rating; // calculate
    private Integer popularity; // calculate

    private Boolean isPostponed; // user info
    private Boolean isCart; // user info

    private Date createdDate;

    @OneToMany(mappedBy = "book")
    private List<Tag> tags = new ArrayList<>();

    private String critique;
    private String description;

}
