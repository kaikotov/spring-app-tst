package com.example.MyBookShoopApp.data.requests;

import lombok.Data;

@Data
public class VerificationRequest {

    private String toNumber;
    private String toMail;
    private String code;
}
