package com.example.MyBookShoopApp.data.requests;

import lombok.Data;


@Data
public class DateFilterRequest {

    private String fromDate;
    private String toDate;
}
