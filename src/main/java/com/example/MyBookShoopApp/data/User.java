package com.example.MyBookShoopApp.data;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private String phone;
    private String mail;

    private Long registrationTime;

//    private List<Integer> purchasedBooksId = new ArrayList<>();
//    private List<Integer> postponedBooksId = new ArrayList<>();
//    private List<Integer> cartBooksId = new ArrayList<>();
}
