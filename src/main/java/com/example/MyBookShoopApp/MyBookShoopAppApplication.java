package com.example.MyBookShoopApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyBookShoopAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyBookShoopAppApplication.class, args);
    }

}
