package com.example.MyBookShoopApp.repositories;

import com.example.MyBookShoopApp.data.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Integer> {
}
