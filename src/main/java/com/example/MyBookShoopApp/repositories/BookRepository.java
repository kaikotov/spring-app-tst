package com.example.MyBookShoopApp.repositories;

import com.example.MyBookShoopApp.data.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {

    List<Book> findBooksByAuthor_FirstName(String name);

    @Query("from Book")
    List<Book> customFindAllBooks();

    List<Book> findBooksByPopularityGreaterThan(Integer popular);

    List<Book> findBooksByCreatedDateBetweenOrderByCreatedDateDesc(Date fromDate, Date toDate);

    List<Book> findBooksByTitleContaining(String query);
}
