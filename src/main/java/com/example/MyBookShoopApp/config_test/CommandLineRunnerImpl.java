package com.example.MyBookShoopApp.config_test;

import com.example.MyBookShoopApp.exception.EntityNotFoundException;
import com.example.MyBookShoopApp.repositories.BookRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class CommandLineRunnerImpl implements CommandLineRunner {

    //    private final TestEntityCrudRepository testEntityCrudRepository;
    private final BookRepository bookRepository;

    @Override
    public void run(String... args) throws Exception {
        for (int i = 0; i < 5; i++) {
            createTestEntity(new TestEntity());
        }

        TestEntity readTestEntity = readTestEntityById(3L);
        if (readTestEntity == null) {
            throw new EntityNotFoundException("Cannot read entity by id");
        }

        TestEntity updatedTestEntity = updateTestEntityById(5L);
        if (updatedTestEntity == null) {
            throw new EntityNotFoundException("Cannot upd entity by id");
        }

        deleteTestEntityById(4L);

//        log.info("filter books bu author (first name): {}", bookRepository.findBooksByAuthor_FirstName("Fidelio"));
//        log.info("find all: {}", bookRepository.customFindAllBooks());

    }

    private void deleteTestEntityById(Long id) {
        TestEntity testEntity = readTestEntityById(id);
//        testEntityCrudRepository.delete(testEntity);
        log.debug("deleteTestEntityById: from DB: {}", readTestEntityById(id));
    }

    private TestEntity updateTestEntityById(Long id) {
        TestEntity testEntity = readTestEntityById(id);
        testEntity.setData("NEW DATA");
//        testEntityCrudRepository.save(testEntity);
        log.debug("updateTestEntityById: from DB: {}", readTestEntityById(id));

        return testEntity;
    }

    private TestEntity readTestEntityById(Long id) {
//        TestEntity testEntity = testEntityCrudRepository.findById(id).orElseGet(TestEntity::new);
//        log.debug("readTestEntityById: from DB: {}", testEntity);
//        return testEntity;
        return new TestEntity();
    }

    private void createTestEntity(TestEntity testEntity) {
        testEntity.setData(testEntity.getClass().getSimpleName() + testEntity.hashCode());
//        testEntityCrudRepository.save(testEntity);
    }
}
