package com.example.MyBookShoopApp.services.books;

import com.example.MyBookShoopApp.repositories.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BooksRatingCalculateService {

    private final BookRepository bookRepository;

    public void calculateBookRating(Integer bookId) {
        // calculate rating: 1 ... 5
        // book.setRating(...)
    }
}
