package com.example.MyBookShoopApp.services.books;

import com.example.MyBookShoopApp.data.Book;
import com.example.MyBookShoopApp.data.requests.DateFilterRequest;
import com.example.MyBookShoopApp.repositories.BookRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class BookService {


    private final BookRepository bookRepository;

    public List<Book> getBooksData() {
        return bookRepository.findAll();
    }

    public List<Book> getPopularBooksData() {
        return bookRepository.findBooksByPopularityGreaterThan(0);
    }

    public List<Book> getRecentBooksData() {
        return bookRepository.findAll(Sort.by(Sort.Direction.DESC, "createdDate"));
    }

    public List<Book> getRecentBooksDataFilteredBooks(DateFilterRequest dateFilterRequest) {
        log.info("sort by date, request{}", dateFilterRequest);
        return bookRepository.findBooksByCreatedDateBetweenOrderByCreatedDateDesc(
                Date.valueOf(dateFilterRequest.getFromDate()),
                Date.valueOf(dateFilterRequest.getToDate()));
    }

    public List<Book> getCartLimitedBooksData() {
        return new ArrayList<>(getBooksData());
    }

    public List<Book> getPostponedLimitedBooksData() {
        return new ArrayList<>(getBooksData());
    }

    public void moveBookToPostponed(Integer id) {
        log.info("update field isPostponed to 'true' for book with id = {}", id);
    }

    public void moveBookToCart(Integer id) {
        log.info("update field isCart to 'true' for book with id = {}", id);
    }

    public List<Book> getFilteredBooksByTitle(String queryStr) {
        return bookRepository.findBooksByTitleContaining(queryStr);
    }
}
