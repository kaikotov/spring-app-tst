package com.example.MyBookShoopApp.services;

import com.example.MyBookShoopApp.data.Author;
import com.example.MyBookShoopApp.repositories.AuthorRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuthorsService {

    private final AuthorRepository authorRepository;

    public List<Author> getAllAuthors() {
        return authorRepository.findAll();
    }

    public Map<String, List<Author>> getGroupsAuthorAlphabetically() {
        List<Author> authors = new ArrayList<>(getAllAuthors());
        Map<String, List<Author>> result = new HashMap<>();
        log.debug("groups authors: {}", result);
        return authors.stream().collect(
                Collectors.groupingBy((Author author) -> author.getLastName().substring(0, 1))
        );
    }
}
