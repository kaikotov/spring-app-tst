package com.example.MyBookShoopApp.services.auth;

import com.example.MyBookShoopApp.constants.Constants;
import com.example.MyBookShoopApp.data.requests.VerificationRequest;
import com.example.MyBookShoopApp.utils.VerificationCodeHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.sql.ResultSet;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class VerificationService {

    private final JdbcTemplate jdbcTemplate;

    /**
     * Generate code and save to DB.
     *
     * @param field               name of field in DB
     * @param verificationRequest request with info of recipient (phone number / email)
     * @return generated code for sending
     */
    public String preSendOperation(String field, VerificationRequest verificationRequest) {
        String codeMsg = VerificationCodeHelper.generateCode();
        String recipient = getRecipient(field, verificationRequest);
        saveCodeToDb(codeMsg, field, recipient);
        return codeMsg;
    }

    /**
     * Validate code with value in DB.
     * Always return false, if field or recipient is empty
     *
     * @param field               name of field in DB
     * @param verificationRequest request with info (code and recipient)
     * @return value of validating code for current recipient
     */
    public boolean validateCode(String field, VerificationRequest verificationRequest) {
        String recipient = getRecipient(field, verificationRequest);

        String codeMsg = verificationRequest.getCode();
        if (StringUtils.isEmpty(recipient) ||
                StringUtils.isEmpty(codeMsg)) {
            log.error("Unable validate code, because code or recipient is empty. Request {}", verificationRequest);
            return false;
        }

        List<String> codes = getCodeValuesFromDb(field, recipient);

        return codes.stream().anyMatch(code -> verificationRequest.getCode().equals(code));
    }

    private String getRecipient(String field, VerificationRequest verificationRequest) {
        return Constants.PHONE_NUMBER_FIELD.equals(field) ?
                verificationRequest.getToMail() :
                verificationRequest.getToMail();
    }

    /**
     * Delete code for current recipient from DB.
     *
     * @param field               name of field in DB
     * @param verificationRequest request with info of recipient (phone number / email)
     */
    public void postValidateOperation(String field, VerificationRequest verificationRequest) {
        String recipient = getRecipient(field, verificationRequest);
        deleteCodeFromDb(field, recipient);
    }


    private void saveCodeToDb(String codeMsg, String field, String recipient) {
        jdbcTemplate.execute(
                "INSERT INTO " + Constants.VERIFICATION_CODES_DB + " (" + field + ", " + Constants.CODE_FIELD + ") " +
                        "values ('" + recipient + "', '" + codeMsg + "')");
    }

    private List<String> getCodeValuesFromDb(String field, String recipient) {
        return jdbcTemplate.query("SELECT * FROM " + Constants.VERIFICATION_CODES_DB + " " +
                        "where " + field + " = '" + recipient + "'",
                (ResultSet rs, int rowNum) -> rs.getString(Constants.CODE_FIELD));
    }

    private void deleteCodeFromDb(String field, String recipient) {
        jdbcTemplate.execute("DELETE FROM " + Constants.VERIFICATION_CODES_DB + " " +
                "WHERE " + field + " = '" + recipient + "';");
    }

}
