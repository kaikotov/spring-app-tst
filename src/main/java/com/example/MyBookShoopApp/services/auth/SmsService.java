package com.example.MyBookShoopApp.services.auth;

import com.example.MyBookShoopApp.constants.Constants;
import com.example.MyBookShoopApp.data.requests.VerificationRequest;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class SmsService {

    @Value("${twilio.account.sid}")
    private String accountSid;

    @Value("${twilio.auth.token}")
    private String authToken;

    @Value("${twilio.phone.number}")
    private String fromNumber;

    private final VerificationService verificationService;

    /**
     * Send sms with code.
     *
     * @param verificationRequest with recipient info
     */
    public void send(VerificationRequest verificationRequest) {
        log.debug("Start send sms to number {}", verificationRequest);
        try {
            String codeMsg = verificationService.preSendOperation(Constants.PHONE_NUMBER_FIELD, verificationRequest);
            Twilio.init(accountSid, authToken);
            Message message = Message.creator(new PhoneNumber(verificationRequest.getToNumber()),
                    new PhoneNumber(fromNumber), codeMsg)
                    .create();

            log.debug("After send: message ID {}, message status {}", message.getSid(), message.getStatus());
        } catch (Exception e) {
            log.error("Unable send sms", e);
        }
    }

    /**
     * Validate code.
     *
     * @param verificationRequest with code and recipient info
     */
    public void validate(VerificationRequest verificationRequest) {
        boolean isValidateCode = verificationService.validateCode(Constants.PHONE_NUMBER_FIELD, verificationRequest);
        if (isValidateCode) {
            log.debug("Code is validate, request {}", verificationRequest);
            verificationService.postValidateOperation(Constants.PHONE_NUMBER_FIELD, verificationRequest);
        } else {
            log.warn("Code is not validate, request {}", verificationRequest);
        }
    }
}
