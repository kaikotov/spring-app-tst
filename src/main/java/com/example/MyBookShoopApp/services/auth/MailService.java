package com.example.MyBookShoopApp.services.auth;

import com.example.MyBookShoopApp.constants.Constants;
import com.example.MyBookShoopApp.data.requests.VerificationRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class MailService {

    private static final String MAIL_SUBJECT = "Verification code";

    private final JavaMailSender javaMailSender;
    private final VerificationService verificationService;


    /**
     * Send email with code.
     *
     * @param verificationRequest recipients info
     */
    public void send(VerificationRequest verificationRequest) {
        try {
            String codeMsg = verificationService.preSendOperation(Constants.MAIL_FIELD, verificationRequest);

            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setSubject(MAIL_SUBJECT);
            mailMessage.setText("Your code: " + codeMsg);
            mailMessage.setTo(verificationRequest.getToMail());
            javaMailSender.send(mailMessage);
        } catch (Exception e) {
            log.error("Unable send email to mail", e);
        }
    }

    /**
     * Validate code.
     *
     * @param verificationRequest with code and recipient info
     */
    public void validate(VerificationRequest verificationRequest) {
        boolean isValidateCode = verificationService.validateCode(Constants.MAIL_FIELD, verificationRequest);
        if (isValidateCode) {
            log.debug("Code is validate, request {}", verificationRequest);
            verificationService.postValidateOperation(Constants.MAIL_FIELD, verificationRequest);
        } else {
            log.warn("Code is not validate, request {}", verificationRequest);
        }
    }
}
