package com.example.MyBookShoopApp.utils;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.util.Random;

@UtilityClass
@Slf4j
public class VerificationCodeHelper {

    public String generateCode() {
        String numbers = "0123456789";
        Random random = new Random();

        char[] password = new char[6];

        for (int i = 0; i < 6; i++) {
            password[i] =
                    numbers.charAt(random.nextInt(numbers.length()));
        }
        return String.valueOf(password);
    }
}
