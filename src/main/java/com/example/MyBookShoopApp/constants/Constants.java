package com.example.MyBookShoopApp.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

    public final String ID_FIELD = "id";
    public final String AUTHOR_ID_FIELD = "author_id";
    public final String TITLE_FIELD = "title";
    public final String PRICE_OLD_FIELD = "price_old";
    public final String PRICE_FIELD = "price";

    public final String FIRST_NAME_FIELD = "first_name";
    public final String LAST_NAME_FIELD = "last_name";


    public final String VERIFICATION_CODES_DB = "verification_codes";
    public final String PHONE_NUMBER_FIELD = "phoneNumber";
    public final String MAIL_FIELD = "mail";
    public final String CODE_FIELD = "code";

    public final String SEND_ENDPOINT = "/send";
    public final String VALIDATE_ENDPOINT = "/validate";

    public final String SMS_ENDPOINT = "/sms";
    public final String SEND_SMS_ENDPOINT = SEND_ENDPOINT + SMS_ENDPOINT;
    public final String VALIDATE_SMS_CODE_ENDPOINT = SMS_ENDPOINT + VALIDATE_ENDPOINT;

    public final String MAIL_ENDPOINT = "/mail";
    public final String SEND_MAIL_ENDPOINT = SEND_ENDPOINT + MAIL_ENDPOINT;
    public final String VALIDATE_MAIL_CODE_ENDPOINT = MAIL_ENDPOINT + VALIDATE_ENDPOINT;

}
