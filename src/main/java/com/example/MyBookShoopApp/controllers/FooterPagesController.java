package com.example.MyBookShoopApp.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
@Slf4j
public class FooterPagesController {

    @GetMapping("/documents")
    public String documentsPage() {
        return "/documents/index";
    }

    @GetMapping("/about")
    public String aboutPage() {
        return "/about";
    }

    @GetMapping("/faq")
    public String faqPage() {
        return "/faq";
    }

    @GetMapping("/contacts")
    public String contactsPage() {
        return "/contacts";
    }

}
