package com.example.MyBookShoopApp.controllers;

import com.example.MyBookShoopApp.data.Author;
import com.example.MyBookShoopApp.services.AuthorsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/authors")
@Slf4j
public class AuthorController {

    private final AuthorsService authorsService;

    @Autowired
    public AuthorController(AuthorsService authorsService) {
        this.authorsService = authorsService;
    }

    @ModelAttribute("authorsMap")
    public Map<String, List<Author>> authorsData() {
        return authorsService.getGroupsAuthorAlphabetically();
    }

    @GetMapping
    public String authorsPage() {
        return "/authors/index";
    }
}
