package com.example.MyBookShoopApp.controllers;

import com.example.MyBookShoopApp.constants.Constants;
import com.example.MyBookShoopApp.data.User;
import com.example.MyBookShoopApp.data.requests.VerificationRequest;
import com.example.MyBookShoopApp.services.auth.MailService;
import com.example.MyBookShoopApp.services.auth.SmsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/signup")
@Slf4j
@RequiredArgsConstructor
public class SignupController {

    private final SmsService smsService;
    private final MailService mailService;

    @ModelAttribute("userForm")
    public User userForm() {
        return new User();
    }

    @ModelAttribute("verificationRequest")
    public VerificationRequest verificationRequest() {
        return new VerificationRequest();
    }

    @GetMapping()
    public String mainPage() {
        return "/signup";
    }

    @PostMapping()
    public String signup(User user) {
        log.info("signup: user for signup {}", user);
        return "/signup";
    }

    /**
     * Send sms with code.
     *
     * @param verificationRequest phone number info
     * @return page
     */
    @PostMapping(Constants.SEND_SMS_ENDPOINT)
    public String sendSms(@RequestBody VerificationRequest verificationRequest) {
        smsService.send(verificationRequest);
        return "/signup";
    }

    /**
     * Validate code.
     *
     * @param verificationRequest phone number and code info
     * @return page
     */
    @PostMapping(Constants.VALIDATE_SMS_CODE_ENDPOINT)
    public String validateSms(@RequestBody VerificationRequest verificationRequest) {
        smsService.validate(verificationRequest);
        return "/signup";
    }

    /**
     * Send sms with code.
     *
     * @param verificationRequest mail info
     * @return page
     */
    @PostMapping(Constants.SEND_MAIL_ENDPOINT)
    public String sendMail(@RequestBody VerificationRequest verificationRequest) {
        mailService.send(verificationRequest);
        return "/signup";
    }

    /**
     * Validate code.
     *
     * @param verificationRequest mail and code info
     * @return page
     */
    @PostMapping(Constants.VALIDATE_MAIL_CODE_ENDPOINT)
    public String validateMail(@RequestBody VerificationRequest verificationRequest) {
        mailService.validate(verificationRequest);
        return "/signup";
    }


}