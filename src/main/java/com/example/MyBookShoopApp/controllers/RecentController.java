package com.example.MyBookShoopApp.controllers;

import com.example.MyBookShoopApp.data.Book;
import com.example.MyBookShoopApp.data.requests.DateFilterRequest;
import com.example.MyBookShoopApp.services.books.BookService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/recent")
@RequiredArgsConstructor
@Slf4j
public class RecentController {

    private final BookService bookService;

    @ModelAttribute("recentSectionBooks")
    public List<Book> recentSectionBooks() {
        return bookService.getRecentBooksData();
    }

    @ModelAttribute("dateFilterRequest")
    public DateFilterRequest dateFilterRequest() {
        return new DateFilterRequest();
    }

    @GetMapping()
    public String mainPage() {
        return "/books/recent";
    }

    @PostMapping("/filter/date")
    public void filterBooksByDate(DateFilterRequest dateFilterRequest, Model model) throws JsonProcessingException {
        log.info("request: {} ", dateFilterRequest);
        List<Book> books = bookService.getRecentBooksDataFilteredBooks(dateFilterRequest);
        model.addAttribute("recentSectionBooks", books);
        log.info("response: {} ", new ObjectMapper().writeValueAsString(books));
    }


}
