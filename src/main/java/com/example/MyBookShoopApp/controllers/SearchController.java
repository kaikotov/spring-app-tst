package com.example.MyBookShoopApp.controllers;

import com.example.MyBookShoopApp.data.Book;
import com.example.MyBookShoopApp.data.requests.QueryRequest;
import com.example.MyBookShoopApp.services.books.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Controller
@RequestMapping("/search")
@Slf4j
@RequiredArgsConstructor
public class SearchController {

    private final BookService bookService;

    @ModelAttribute("searchBooks")
    public List<Book> searchBooks() {
        return new ArrayList<>();
    }

    @ModelAttribute("queryRequest")
    public QueryRequest queryRequest() {
        return new QueryRequest();
    }

    @GetMapping(value = {"/", "/{queryStr}"})
    public String mainPage(@PathVariable(value = "queryStr", required = false) Optional<String> queryStr, Model model) {
        log.info("Query {}", queryStr);
        if (queryStr.isPresent()) {
            model.addAttribute("searchBooks", bookService.getFilteredBooksByTitle(queryStr.get()));
        } else {
            model.addAttribute("searchBooks", new ArrayList<>());
        }
        return "/search/index";
    }

    @PostMapping()
    public String searchBooks(@ModelAttribute QueryRequest queryRequest, Model model) {
        log.info("Query1 {}", queryRequest.getQuery());
        if (Objects.nonNull(queryRequest.getQuery())) {
            model.addAttribute("searchBooks",
                    bookService.getFilteredBooksByTitle(queryRequest.getQuery()));
        } else {
            model.addAttribute("searchBooks", new ArrayList<>());
        }
        return "/search/index";
    }
}