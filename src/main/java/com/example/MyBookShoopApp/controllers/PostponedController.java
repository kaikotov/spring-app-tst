package com.example.MyBookShoopApp.controllers;

import com.example.MyBookShoopApp.data.Book;
import com.example.MyBookShoopApp.services.books.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/postponed")
@Slf4j
@RequiredArgsConstructor
public class PostponedController {

    private final BookService bookService;

    @ModelAttribute("booksData")
    public List<Book> booksData() {
        return bookService.getPostponedLimitedBooksData();
    }

    @GetMapping()
    public String mainPage() {
        return "/postponed";
    }

    @PostMapping
    public String moveBookToPostponed() {
//        bookService.moveBookToPostponed();
        return "/cart";
    }
}
