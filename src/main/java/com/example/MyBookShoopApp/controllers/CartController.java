package com.example.MyBookShoopApp.controllers;

import com.example.MyBookShoopApp.data.Book;
import com.example.MyBookShoopApp.services.books.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/cart")
@Slf4j
@RequiredArgsConstructor
public class CartController {

    private final BookService bookService;

    @ModelAttribute("booksData")
    public List<Book> booksData() {
        return bookService.getCartLimitedBooksData();
    }

    @GetMapping()
    public String mainPage() {
        return "/cart";
    }

    @PostMapping
    public String moveBookToCart() {
//        bookService.moveBookToCart();
        return "/cart";
    }
}
