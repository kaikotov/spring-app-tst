package com.example.MyBookShoopApp.controllers;

import com.example.MyBookShoopApp.data.Book;
import com.example.MyBookShoopApp.services.books.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/popular")
@RequiredArgsConstructor
@Slf4j
public class PopularController {

    private final BookService bookService;

    @ModelAttribute("popularSectionBooks")
    public List<Book> popularSectionBooks() {
        return bookService.getPopularBooksData();
    }

    @GetMapping()
    public String mainPage() {
        return "/books/popular";
    }
}
